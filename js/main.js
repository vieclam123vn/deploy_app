function scrollAllJobs() {
    var startTime = new Date().getTime();
    var interval = setInterval(function () {

        var excludedJobs = document.querySelectorAll('div[data-pagelet=page] div[aria-label="Việc làm tại các doanh nghiệp quanh đây"] div.scb9dxdr div.j83agx80 span.hpfvmrgz a:nth-child(2)[href^="/job"]');
        excludedJobs.forEach(el => {
            el.closest("div.scb9dxdr").remove();
        });

        var jobs = document.querySelectorAll('div[data-pagelet=page] div[aria-label="Việc làm tại các doanh nghiệp quanh đây"] div.scb9dxdr div.j83agx80 span.hpfvmrgz a:nth-child(1)[href^="/job"]');
        jobs.forEach(value => {
            value.setAttribute('data-job', 1);
        });
        var currentTime = new Date().getTime();
        if (currentTime - startTime > 10 * 60 * 1000) {
            clearInterval(interval);
        } else {
            window.scrollTo(0, document.body.scrollHeight);
        }
    }, 2000);
}

scrollAllJobs();

